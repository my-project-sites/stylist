<?php

/*
    Plugin Name: User data
    Description: Плагин пользовательских данных
    Version: 1.0
    Author: Aleksandr Tsapin
    Author URI: https://www.facebook.com/profile.php?id=100004950068062


    Copyright 2018  Aleksandr Tsapin  (email: tsapin89@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

include_once 'functions.php';

/*Привязываем функции активации, деактивации, удаления*/
register_activation_hook(__FILE__, 'AT_install');
register_deactivation_hook(__FILE__, 'AT_deinstall');
register_uninstall_hook(__FILE__, 'AT_uninstall');

/*Привязываем функцию добавления нового раздела в меню*/
add_action('admin_menu', 'AT_addSection');

/*Получим шорткоды для пользовательских данных*/
add_shortcode('userEmail', 'AT_email');

add_shortcode('userPhone', 'AT_phone');
add_shortcode('userPhone2', 'AT_phone2');
add_shortcode('userPhone3', 'AT_phone3');

add_shortcode('userChart', 'AT_chart');
add_shortcode('userCatalog', 'AT_catalog');

add_shortcode('userAddress', 'AT_address');
add_shortcode('userVK', 'AT_vk');
add_shortcode('userFacebook', 'AT_facebook');
add_shortcode('userYoutube', 'AT_youtube');
add_shortcode('userInstagram', 'AT_instagram');
add_shortcode('userTelegram', 'AT_telegram');
add_shortcode('userBehance', 'AT_behance');
add_shortcode('userTwitter', 'AT_twitter');

?>