<style>

.AT_h3 {
	font-size: 20px;
}

.AT_success {
	color: green;
	font-size: 20px;
	text-decoration: underline;
}

.AT_danger {
	color: red;
	font-size: 20px;
	text-decoration: underline;
}

.AT_label {
	font-weight: bold;
	font-size: 16px;
}

.AT_input {
	margin-top: 2px;
	width: 600px;
	height: 30px;
	display: block;
}

.AT_button {
	margin-top: 10px;
	height: 30px;
	font-weight: bold;
}
	
</style>


<div id="AT_content">
	<h3 class="AT_h3">Введите свои контактные данные</h3>

	<form class="AT_form" method="post">
		<label class="AT_label">Каталог</label>
		<input class="AT_input" type="text" name="catalog" value="<?=$data['catalog'];?>">

		<label class="AT_label">Адрес</label>
		<input class="AT_input" type="text" name="address" value="<?=$data['address'];?>">

		<label class="AT_label">email</label>
		<input class="AT_input" type="text" name="email" value="<?=$data['email'];?>">

		<label class="AT_label">Офис</label>
		<input class="AT_input" type="text" name="phone" value="<?=$data['phone'];?>">

		<label class="AT_label">Отдел продаж</label>
		<input class="AT_input" type="text" name="phone2" value="<?=$data['phone2'];?>">

		<label class="AT_label">Главный инженер</label>
		<input class="AT_input" type="text" name="phone3" value="<?=$data['phone3'];?>">

		<label class="AT_label">График работы</label>
		<input class="AT_input" type="text" name="chart" value="<?=$data['chart'];?>">

		<label class="AT_label">Вконтакте</label>
		<input class="AT_input" type="text" name="vk" value="<?=$data['vk'];?>">

		<label class="AT_label">Facebook</label>
		<input class="AT_input" type="text" name="facebook" value="<?=$data['facebook'];?>">

		<label class="AT_label">Youtube</label>
		<input class="AT_input" type="text" name="youtube" value="<?=$data['youtube'];?>">

		<label class="AT_label">Instagram</label>
		<input class="AT_input" type="text" name="instagram" value="<?=$data['instagram'];?>">

		<label class="AT_label">Telegram</label>
		<input class="AT_input" type="text" name="telegram" value="<?=$data['telegram'];?>">

		<label class="AT_label">Behance</label>
		<input class="AT_input" type="text" name="behance" value="<?=$data['behance'];?>">

		<label class="AT_label">Twitter</label>
		<input class="AT_input" type="text" name="twitter" value="<?=$data['twitter'];?>">

		<button class="AT_button" type="submit" name="add" class="btn btn-primary">Отправить</button>
	</form>
</div>