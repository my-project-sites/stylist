<?php 
	
	include 'header.php'; 
	wp_reset_postdata(); 
?>

<body>
	<div id="pagepiling">
	    <div class="section" id="private" data-anchor="private">
	      <div class="container-fluid">
	        <div class="row">
	        	
	          <div class="offset-lg-1 col-lg-10">
	          	<div class="content">
	          		<h1><?php the_title();?></h1>
					<?php the_content();?>
				</div>	
	          </div>

	        </div> <!-- End row -->
	      </div> <!-- End container -->
	    </div>
<?php 
	/*Переменная для верхней конпки футера*/
	$link = 'private'; 
?>

<?php include 'footer.php' ?>