<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?=get_template_directory_uri();?>/assets/images/favicon.ico" type="image/x-icon">
	<title>Style for you</title>

	<?php wp_head(); ?>
</head>