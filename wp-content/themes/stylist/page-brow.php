<?php 
	get_header();
	wp_reset_postdata();
?>

<body>
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section" id="brow" data-anchor="brow">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="offset-lg-1 col-lg-10">
			          	<div class="content AnimationTop">
			          		<h1><?php the_title();?></h1>
							<?php the_content();?>
						</div>	
			          </div>	
	    		</div> <!-- End row -->
	    	</div> <!-- End container -->
	    </div>
<?php 
	/*Переменная для верхней конпки футера*/
	$link = 'brow'; 
?>

<?php include 'footer.php'; ?>