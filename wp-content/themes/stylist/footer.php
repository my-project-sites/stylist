<footer class="section">
	<a id="ButtonUp" href="#<?=$link?>" data-menuanchor="<?=$link?>" class="S6_button text_regular">Вверх</a>

	<div class="container-fluid">
		<div class="row">	
			<div class="col-11 offset-md-2 col-md-2">
				<img class="footer_img img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/bourjois.png" alt="bourjois">
			</div>

			<div class="col-11 col-md-2">
				<img class="footer_img img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/schwarzkopf.png" alt="schwarzkopf">
			</div>

			<div class="col-11 col-md-2">
				<img class="footer_img img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/avon.png" alt="avon">
			</div>

			<div class="col-11 col-md-2">
				<img class="footer_img img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/loreal.png" alt="loreal">
			</div>
		</div> <!-- End row -->

		<div class="row">
			<div class="offset-md-1 col-md-10">
				<hr>
			</div>
		</div>

		<div class="row">
			<div class="offset-lg-2 col-lg-8">
				<div class="footer_menu">
    				<ul class="footer_menu-list list-unstyled list-inline">
    					<li class="list-inline-item">
    						<a class="footer_menu-link" href="<?php echo get_home_url();?>">Главная</a>
    					</li>

    					<li class="list-inline-item">
    						<a class="footer_menu-link" href="<?php echo get_home_url();?>/price">Прайс</a>
    						<ul class="footer_submenu list-unstyled">
		    					<li>
		    						<a href="<?php echo get_home_url();?>/stylist" class="text_light">Стилист</a>
		    					</li>
		    					<li>
		    						<a href="<?php echo get_home_url();?>/brow" class="text_light">Бровист</a>
		    					</li>
		    				</ul>
    					</li>

    					<li class="list-inline-item">
    						<a class="footer_menu-link" href="<?php echo get_home_url();?>/entry">Запись</a>
    						<ul class="footer_submenu list-unstyled">
		    					<li>
		    						<a class="text_light" href="<?php echo get_home_url();?>/entry#calendar">Посмотреть календарь</a>
		    					</li>

		    					<li>
		    						<a class="text_light" href="<?php echo get_home_url();?>/entry#note">Записаться</a>
		    					</li>
		    				</ul>
    					</li>

    					<li class="list-inline-item">
    						<a class="footer_menu-link" href="<?php echo get_home_url();?>/gallery">Галерея</a>
    						<ul class="footer_submenu list-unstyled">
		    					<li>
		    						<a target="_blank" href="<?=do_shortcode('[userInstagram]');?>" class="text_light">Инстаграм</a>
		    					</li>
		    				</ul>
    					</li>

    					<li class="list-inline-item">
    						<a class="footer_menu-link" href="<?php echo get_home_url();?>/about">О себе</a>
    						<ul class="footer_submenu list-unstyled">
		    					<li>
		    						<a onclick="get_hair()" href="<?php echo get_home_url();?>/private-policy" class="text_light attention">Политика<br>конфиденциальности</a>
		    					</li>

		    				</ul>
    					</li>

    					<li class="list-inline-item">
    						<a class="footer_menu-link" href="<?php echo get_home_url();?>/contacts">Контакты</a>
    						<ul class="footer_submenu list-unstyled">
		    					<li>
		    						<a class="text_light" data-toggle="modal" data-target="#ModalFirst" href="#">Оставить сообщение</a>
		    					</li>
		    				</ul>
    					</li>
    				</ul>
				</div>	
			</div>
		</div> <!-- End row -->

		<div class="row">
			<div class="col-md-12">
				<div class="footer_author">
					<p class="text_light">Разработка сайта:</p>
					<div class="footer_design">
						<a target="_blank" href="https://www.facebook.com/bloodborne.gothic/">
							<img class="my-logo" src="<?=get_template_directory_uri();?>/assets/images/my-logo.png" alt="mylogo">
						</a>
					</div>
					<p class="footer_copy">All rights reserved &amp; copy</p>
				</div>
			</div>
		</div>

	</div> <!-- End container -->	
</footer>
</div> <!-- End fullpage -->

<?php wp_footer(); ?>

<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- Modal -->
<div class="modal fade" id="ModalFirst" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
      	<?php echo do_shortcode('[contact-form-7 id="428" title="Форма модального окна"]'); ?>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

<script>
	if(document.documentElement.clientWidth < 1300) {
		location.href = 'http://m.stylist.host';
	}
</script>

</body>
</html>