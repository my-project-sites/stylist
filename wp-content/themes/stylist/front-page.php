<?php
	get_header();
	wp_reset_postdata();
?>

<body>	
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <header id="home" class="section" data-anchor="home">
	      <div class="container-fluid">
	        <div class="row">
				<div class="offset-lg-1 col-lg-2">
					<a href="/">
						<img class="logo img-fluid" src="<?=get_template_directory_uri();?>/assets/images/logo.png" alt="logo">
					</a>
				</div>
				
				<div class="col-lg-8">
					<ul id="topmenu" class="list-inline list-unstyled float-right">
						<li class="top-active list-inline-item" data-menuanchor="home">
							<a href="#home">Главная</a>
						</li>
						<li class="list-inline-item" data-menuanchor="services">
							<a href="#services">Услуги</a>
						</li>
						<li class="list-inline-item" data-menuanchor="entry">
							<a href="#entry">Запись</a>
						</li>
						<li class="list-inline-item" data-menuanchor="gallery">
							<a href="#gallery">Галерея</a>
						</li>
						<li class="list-inline-item" data-menuanchor="about">
							<a href="#about">О нас</a>
						</li>
						<li class="list-inline-item" data-menuanchor="contacts">
							<a href="#contacts">Контакты</a>
						</li>
					</ul>
				</div> <!-- End div -->
	       
	          <div class="offset-md-6 col-md-5">
	          	<div class="header_text AnimationRight">
	          		<?php the_content(); ?>
	          	</div>	
	          </div>

	        </div> <!-- End row -->
	      </div> <!-- End container -->
	    </header>

	    <div class="section" id="services" data-anchor="services">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-md-12">
	    				<div class="services_text">
			          		<p class="services_text-first text_regular">Наш салон красоты предоставит вам отличные услуги по разумной цене. С нами вы всегда будете неотразимы.</p>
			          		<p class="text_regular">Ознакомьтесь с нашими услугами.</p>	
			          	</div>	
	    			</div>
	    		</div> <!-- End row -->

	    		<div class="row">
	    			<div class="offset-md-1 col-md-10">
	    				<div class="buttons">
	    					<a class="services_button button" href="<?=get_home_url();?>/brow">Бровист</a>
	    					<a class="services_button button" href="<?=get_home_url();?>/price">Прайс</a>
	    					<a class="services_button button" href="<?=get_home_url();?>/stylist">Стилист</a>
	    				</div>
	    			</div>
	    		</div>
	    	</div> <!-- End container -->
	    </div>
	 
	    <div class="section" id="entry" data-anchor="entry">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="offset-md-1 col-md-5">
	    				<div class="entry_text-left">
	    					<p class="text_light">Ознакомьтесь с календарем и оставьте заявку на удобное для вас время. После чего с вами свяжутся и уточнят все детали.</p>
	    					<a class="entry_button button float-left" href="<?php echo get_home_url();?>/entry">Записаться</a>
	    				</div>	
	    			</div> <!-- End col-md-6 -->

	    			<div class="col-md-5">
	    				<div class="entry_text-right">
	    					<p class="text_regular">Записывайтесь когда вам удобно.</p>
	    					<p class="text_light">Наши мастера имеют гибкий график работы!</p>
	    				</div>
	    			</div> <!-- End col-md-6 -->
	    			
	    		</div> <!-- End row -->
	    	</div> <!-- End container -->
	    </div>

	    <div class="section" id="gallery" data-anchor="gallery">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="offset-lg-1 col-lg-5">
	    				<div class="gallery_text-left">
	    					<p class="text_regular">Избранные фото</p>
	    					<p class="text_light gallery_text-second">Мы собрали для вас лучшие образцы наших работ.</p>
	    					<a class="gallery_button button" href="<?php echo get_home_url();?>/gallery">Посмотреть фото</a>	
	    				</div>	
	    			</div>

    				<div class="col-lg-5">
	    				<div class="gallery_text-right">
	    					<p class="text_light">Так же не забывайте подписаться на инстаграм 😉</p>
	    				</div>
	    			</div>
	    		</div> <!-- End row -->
	    	</div> <!-- End container -->
	    </div>

	    <div class="section" id="about" data-anchor="about">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="offset-lg-1 col-lg-5">
	    				<div class="about_text-left">
			          		<p class="text_regular">Немного обо мне</p>
			          		<p class="about_text-second text_light">Здесь я хочу поделиться с вами тем, что сподвигло меня встать на тот путь по которому иду.</p>
			          		<a class="about_button button float-left" href="<?php echo get_home_url();?>/about">Прочитать</a>	
			          	</div>	
	    			</div> <!-- End col-md-12 -->

	    			<div class="col-lg-5">
	    				<div class="about_text-right">
			          		<p class="text_regular">Контактная информация</p>
			          		<p class="about_text-second text_light">Если вы не знаете как к нам попасть, то ознакомтесь, пожалуйста, с нашими контактными данными. Также подпишитесь на наши соцсети.</p>
			          		<a class="about_button button float-right" href="<?php echo get_home_url();?>/contacts">Контакты</a>	
			          	</div>	
	    			</div> <!-- End col-md-12 -->
	    		</div> <!-- End row -->
	    	</div> <!-- End container -->
	    </div>

<?php 
	// Переменная для верхней конпки футера
	$link = 'home'; 
?>

<?php include 'footer.php'; ?>