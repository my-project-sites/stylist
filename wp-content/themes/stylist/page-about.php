<?php 
	get_header();
	wp_reset_postdata();
?>

<body>
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section" id="greeting" data-anchor="greeting">
	      <div class="container-fluid">
	        <div class="row">
	          <div class="offset-md-6 col-md-5">
	          	<div class="greeting_block-right AnimationRight">
	          		<h1>Давайте знакомиться!</h1>
	          		<p>Меня зовут Ирина и я с самого детства любила стоять у зеркала в маминых туфлях и красила губы ее помадами. Навсегда останутся в памяти тени всех оттенков синего и серого, как же это было красиво 😉
					Время шло, но любовь к красоте становилась только сильнее и все больше притягивала меня.</p>  	
	          	</div>	
	          </div>
	        </div> <!-- End row -->
	      </div> <!-- End container -->
	    </div>

	    <div class="section" id="aboutMe" data-anchor="aboutMe">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="offset-lg-1 col-lg-10">
	    				<div class="aboutMe_block">
	    					<p class="text_light"></p>
	    					<div class="content AnimationTop">
								<?php the_content();?>
							</div>
	    				</div>	
	    			</div>	
	    		</div> <!-- End row -->
	    	</div> <!-- End container -->
	    </div>
	
<?php 
	/*Переменная для верхней конпки футера*/
	$link = 'greeting'; 
?>

<?php include 'footer.php'; ?>