<?php 
	get_header();
	wp_reset_postdata();
?>

<body>
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section" id="calendar" data-anchor="calendar">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-md-8">
	    				<h1>Ознакомьтесь с расписание и оставьте заявку</h1>
			          	<div class="note_block">
			          		<?php echo do_shortcode('[my_calendar format="calendar" time="month" above="nav,toggle,jump,print,timeframe" below="key,feeds"]');?>
			          	</div>	
			        </div>
	    		</div>
	    	</div>
	    </div>

	    <div class="section" id="note" data-anchor="note">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-lg-6">
			          	<div class="note_block">
			          		<?php echo do_shortcode('[contact-form-7 id="398" title="Форма обратного звонка"]');?>
			          	</div>	
			        </div>
	    		</div>
	    	</div>	
	    </div>
<?php 
	/*Переменная для верхней кнопки футера*/
	$link = 'calendar'; 
?>

<?php include 'footer.php'; ?>