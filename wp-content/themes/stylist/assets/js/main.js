$(document).ready(function() {

	/*Функции добавления классов анимации*/
	function get_animation_services() {
	    if ($('body').hasClass('pp-viewing-services')) {
	        $('.services_text').addClass('AnimationTop'); 
	        $('.buttons').addClass('AnimationDown');   
	    }
	    else {
	        setTimeout(get_animation_services, 100);
	    }
	}

	function get_animation_entry() {
	    if ($('body').hasClass('pp-viewing-entry')) {
	    	$('.entry_text-left').addClass('AnimationLeft');
	        $('.entry_text-right').addClass('AnimationRight');
	    }
	    else {
	        setTimeout(get_animation_entry, 100);
	    }
	}

	function get_animation_gallery() {
	    if ($('body').hasClass('pp-viewing-gallery')) {
	        $('.gallery_text-left').addClass('AnimationLeft');
	        $('.gallery_text-right').addClass('AnimationRight');
	    }
	    else {
	        setTimeout(get_animation_gallery, 100);
	    }
	}

	function get_animation_about() {
	    if ($('body').hasClass('pp-viewing-about')) {
	        $('.about_text-left').addClass('AnimationLeft');
	        $('.about_text-right').addClass('AnimationRight');
	    }
	    else {
	        setTimeout(get_animation_about, 100);
	    }
	}

	/*Запускаем анимацию*/
	get_animation_services();
	get_animation_entry();
	get_animation_gallery();
	get_animation_about();
	
}); /*End ready*/