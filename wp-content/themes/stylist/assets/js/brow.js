$(document).ready(function() {

	$('#pagepiling').pagepiling({
		menu: '#topmenu',
		anchors: ['brow', 'contacts'],
		navigation: {
	    	'textColor': '#f2f2f2',
	        'bulletsColor': 'white',
	        'position': 'right'
	    }
	}); /*End pagepiling*/

}); /*End ready*/