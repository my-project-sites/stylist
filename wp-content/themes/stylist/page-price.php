<?php 
	get_header();
	wp_reset_postdata();
?>

<body>
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section" id="price" data-anchor="price">
	      <div class="container-fluid">
	        <div class="row">
	          <div class="offset-lg-2 col-lg-8">
	          	<div class="content services_block AnimationTop">
	          		<h1><?php the_title();?></h1>
					<?php the_content();?>
				</div>	
	          </div>
	        </div> <!-- End row -->
	      </div> <!-- End container -->
	    </div>
<?php 
	/*Переменная для верхней конпки футера*/
	$link = 'price'; 
?>

<?php include 'footer.php'; ?>