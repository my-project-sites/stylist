<?php
# Шорткод для home_url
function url_func() {
    return get_home_url();
}
add_shortcode( 'main_url', 'url_func' );

# Шорткод для site_name
function site_name() {
    $name = get_home_url();
    $number = stripos($name, '//');
    return 'www.' . substr($name, $number + 2);
}
add_shortcode( 'site_name', 'site_name' );

# Уберем/покажем admin-бар
add_filter('show_admin_bar', '__return_false');

# Подключаем стили в шапке
add_action('wp_enqueue_scripts', 'header_styles');
function header_styles() {
	wp_enqueue_style('pagepiling', get_template_directory_uri() . '/assets/css/jquery.pagepiling.css');
}

# Подключаем стили в футере
add_action('wp_footer', 'footer_styles');
function footer_styles() {
	wp_enqueue_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');
	wp_enqueue_style('1366', get_template_directory_uri() . '/assets/css/1366.css', array(), false, 
	'(min-width: 1301px) and (max-width: 1900px)');
	wp_enqueue_style('style', get_stylesheet_uri());

	# Прайс
	if(is_page(5)) {wp_enqueue_style('price', get_template_directory_uri() . '/assets/css/price.css');}
	# Галерея
	if(is_page(10)) {wp_enqueue_style('gallery', get_template_directory_uri() . '/assets/css/gallery.css');}
	# Контакты
	if(is_page(306)) {wp_enqueue_style('contacts', get_template_directory_uri() . '/assets/css/contacts.css');}
	# Запись
	if(is_page(315)) {wp_enqueue_style('entry', get_template_directory_uri() . '/assets/css/entry.css');}
	# Политика приватности
	if(is_page(429)) {wp_enqueue_style('private', get_template_directory_uri() . '/assets/css/private.css');}
	# stylist
	if(is_page(475)) {wp_enqueue_style('stylist', get_template_directory_uri() . '/assets/css/stylist.css');}
	# brow
	if(is_page(477)) {wp_enqueue_style('brow', get_template_directory_uri() . '/assets/css/brow.css');}
	# 404
	if(is_404()) {wp_enqueue_style('404', get_template_directory_uri() . '/assets/css/404.css');}

	wp_enqueue_style('Arimo', '//fonts.googleapis.com/css?family=Arimo:400,700&amp;subset=cyrillic');
	wp_enqueue_style('Fira+Sans', '//fonts.googleapis.com/css?family=Fira+Sans:300,400');
	wp_enqueue_style('Exo', '//fonts.googleapis.com/css?family=Exo+2:300&amp;subset=cyrillic');
}

# Подключаем скрипты в шапке
add_action('wp_enqueue_scripts', 'header_scripts');
function header_scripts() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery-1.12.4.js');
	wp_enqueue_script('jquery');

	wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js');
	wp_enqueue_script('pagepiling', get_template_directory_uri() . '/assets/js/jquery.pagepiling.js');
	wp_enqueue_script('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
	wp_enqueue_script('popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
	wp_enqueue_script('recaptcha', '//www.google.com/recaptcha/api.js');

	# Подключим персональные скрипты страниц
	if(is_page(450)) {wp_enqueue_script('home', get_template_directory_uri() . '/assets/js/home.js');}
	# Прайс
	if(is_page(5)) {wp_enqueue_script("price", get_template_directory_uri() . '/assets/js/price.js');}
	# gallery
	if(is_page(10)) {wp_enqueue_script("gallery", get_template_directory_uri() . '/assets/js/gallery.js');}
	# contacts
	if(is_page(306)) {wp_enqueue_script("contacts", get_template_directory_uri() . '/assets/js/contacts.js');}
	# about
	if(is_page(310)) {wp_enqueue_script("about", get_template_directory_uri() . '/assets/js/about.js');}
	# entry
	if(is_page(315)) {wp_enqueue_script("entry", get_template_directory_uri() . '/assets/js/entry.js');}
	# Политика приватности
	if(is_page(429)) {wp_enqueue_script("private", get_template_directory_uri() . '/assets/js/private.js');}
	# Стилист
	if(is_page(475)) {wp_enqueue_script("stylist", get_template_directory_uri() . '/assets/js/stylist.js');}
	# Бровист
	if(is_page(477)) {wp_enqueue_script("brow", get_template_directory_uri() . '/assets/js/brow.js');}
	# Бровист
	if(is_404()) {wp_enqueue_script("404", get_template_directory_uri() . '/assets/js/404.js');}
}

# Подключаем скрипты в футере
add_action('wp_footer', 'footer_scripts');
function footer_scripts() {
	wp_enqueue_script('fontawesome', '//use.fontawesome.com/releases/v5.0.8/js/all.js');
}

# Удалить атрибут type у scripts и styles
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
    return str_replace("type='text/css'", '', $src);
}
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
    return str_replace("type='text/javascript'", '', $src);
}

# Уберем тег p вокруг изображений
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

?>